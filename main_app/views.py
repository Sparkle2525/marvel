from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from main_app.models import Comic, Variant, Story, Character
from main_app.api import ComicSerializer
from main_app.tasks import make_order


@csrf_exempt
def exist(request):
    title = request.GET.get('title', '')
    if_exist = Comic.objects.filter(title=title).exists()
    return JsonResponse({'exists': if_exist})


@csrf_exempt
def comics_list(request):
    if request.method == 'GET':
        display = request.GET.get('display', '')
        if display == 'on':
            comics = Comic.objects.filter(active=True)
        else:
            comics = Comic.objects.all()
        serializer = ComicSerializer(comics.order_by('-creation'), many=True)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = ComicSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)


@csrf_exempt
def comic_detail(request, pk):
    try:
        comic = Comic.objects.get(pk=pk)
    except Comic.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = ComicSerializer(comic)
        return JsonResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        variants_to_remove = data.get('variantsToRemove')
        stories_to_remove = data.get('storiesToRemove')
        characters_to_remove = data.get('charactersToRemove')

        variants = data.get('variants')
        if variants:
            variants = filter(lambda v: v['id'] not in variants_to_remove, variants)
            data['variants'] = list(variants)

            Variant.objects.filter(id__in=variants_to_remove).delete()

        stories = data.get('stories')
        if stories:
            stories = filter(lambda s: s['id'] not in stories_to_remove, stories)
            data['stories'] = list(stories)

            Story.objects.filter(id__in=stories_to_remove).delete()

        characters = data.get('characters')
        if characters:
            characters = filter(lambda s: s['id'] not in characters_to_remove, characters)
            data['characters'] = list(characters)

            Character.objects.filter(id__in=characters_to_remove).delete()
        serializer = ComicSerializer(comic, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        comic.delete()
        return HttpResponse(status=204)


@csrf_exempt
def order_comic(request):
    data = JSONParser().parse(request)
    comic_id = data['id']
    email = data['email']
    make_order.delay(comic_id, email)
    return HttpResponse(status=200)