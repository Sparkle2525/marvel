from __future__ import absolute_import

from celery import shared_task
from main_app.models import Order, Comic
from django.core.mail import send_mail


@shared_task
def make_order(comic_id, email):
    comic = Comic.objects.get(id=comic_id)
    Order.objects.create(comic=comic, email=email)
    message = u'Ваш заказ на комикс %s принят!' % (comic.title,)
    send_mail('Заказ принят!', message, 'icl.test@yandex.ru', [email])

