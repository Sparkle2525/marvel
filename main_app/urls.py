# Routers provide an easy way of automatically determining the URL conf.
from django.conf.urls import url, include
from rest_framework import routers
from main_app import views

from main_app.api import ComicViewSet

router = routers.DefaultRouter()
router.register(r'comics', ComicViewSet)

urlpatterns = [
    url(r'^comics/(?P<pk>[0-9]+)$', views.comic_detail),
    url(r'^if_exist', views.exist),
    url(r'^comics', views.comics_list),
    url(r'^order', views.order_comic)
]
