from rest_framework import serializers, viewsets

from main_app.models import Comic, Variant, Image, Character, Story, Thumbnail


class VariantSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(required=False)

    class Meta:
        model = Variant
        fields = ('id', 'name')


class ImageSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Image
        fields = ('path', 'extension')


class CharacterSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(required=False)

    class Meta:
        model = Character
        fields = ('id', 'name', 'role')


class StorySerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(required=False)

    class Meta:
        model = Story
        fields = ('id', 'name', 'type')


class ThumbnailSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Thumbnail
        fields = ('path', 'extension')


class ComicSerializer(serializers.HyperlinkedModelSerializer):
    variants = VariantSerializer(many=True)
    images = ImageSerializer(many=True)
    characters = CharacterSerializer(many=True)
    stories = StorySerializer(many=True)
    thumbnail = ThumbnailSerializer()

    class Meta:
        model = Comic
        fields = ('id', 'description', 'title', 'date', 'ean', 'active',
                  'thumbnail', 'variants', 'images', 'characters', 'stories')

    def update(self, instance, validated_data):
        instance.description = validated_data.get('description', instance.description)
        instance.title = validated_data.get('title', instance.title)
        instance.date = validated_data.get('date', instance.date)
        instance.ean = validated_data.get('ean', instance.ean)
        instance.active = validated_data.get('active', instance.active)

        variants = validated_data.pop('variants')
        characters = validated_data.pop('characters')
        stories = validated_data.pop('stories')

        for variant in variants:
            variant = dict(**variant)
            v = Variant.objects.get(id=variant['id'])
            v.name = variant['name']
            v.save()

        for character in characters:
            character = dict(**character)
            c = Character.objects.get(id=character['id'])
            c.name = character['name']
            c.role = character['role']
            c.save()

        for story in stories:
            story = dict(**story)
            s = Story.objects.get(id=story['id'])
            s.name = story['name']
            s.type = story['type']
            s.save()

        instance.save()
        return instance

    def create(self, validated_data):
        variants = validated_data.pop('variants')
        images = validated_data.pop('images')
        characters = validated_data.pop('characters')
        stories = validated_data.pop('stories')
        thumbnail = validated_data.pop('thumbnail')
        thumbnail = Thumbnail.objects.create(**thumbnail)
        comic = Comic.objects.create(thumbnail=thumbnail, **validated_data)

        for variant in variants:
            Variant.objects.create(comic=comic, **variant)
        for image in images:
            Image.objects.create(comic=comic, **image)
        for character in characters:
            Character.objects.create(comic=comic, **character)
        for story in stories:
            Story.objects.create(comic=comic, **story)
        return comic


class ComicViewSet(viewsets.ModelViewSet):
    queryset = Comic.objects.all()
    serializer_class = ComicSerializer
