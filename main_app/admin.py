from django.contrib import admin

# Register your models here.
from main_app.models import Comic, Image, Variant, Character, Story, Order


class ImagesInline(admin.TabularInline):
    model = Image
    extra = 0


class VariantsInline(admin.TabularInline):
    model = Variant
    extra = 0


class CharactersInline(admin.TabularInline):
    model = Character
    extra = 0


class StoriesInline(admin.TabularInline):
    model = Story
    extra = 0


class ComicAdmin(admin.ModelAdmin):
    inlines = [
        ImagesInline,
        VariantsInline,
        CharactersInline,
        StoriesInline
    ]

admin.site.register(Comic, ComicAdmin)
admin.site.register(Order)

