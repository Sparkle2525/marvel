from django.apps import AppConfig


class MarvelAppConfig(AppConfig):
    name = 'main_app'
