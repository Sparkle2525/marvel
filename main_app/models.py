from django.db import models


class Thumbnail(models.Model):
    path = models.TextField(verbose_name='Путь')
    extension = models.TextField(verbose_name='Расширение')

    def __str__(self):
        return self.path + '.' + self.extension


class Comic(models.Model):
    description = models.TextField(null=True, blank=True, verbose_name='Описание')
    title = models.TextField(verbose_name='Заголовок')
    date = models.DateTimeField(null=True, blank=True, verbose_name='Дата выпуска')
    ean = models.TextField(null=True, blank=True, verbose_name='EAN')
    thumbnail = models.ForeignKey(Thumbnail, verbose_name='Миниатюра')
    active = models.BooleanField(default=True, verbose_name='Показывать на сайте')
    creation = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title


class Variant(models.Model):
    comic = models.ForeignKey(Comic, related_name='variants')
    name = models.TextField(verbose_name='Наименование варианта')

    def __str__(self):
        return self.name


class Image(models.Model):
    comic = models.ForeignKey(Comic, related_name='images')
    path = models.TextField(verbose_name='Путь')
    extension = models.TextField(verbose_name='Расширение')

    def __str__(self):
        return self.path + '.' + self.extension


class Character(models.Model):
    comic = models.ForeignKey(Comic, related_name='characters')
    name = models.TextField(verbose_name='Имя персонажа')
    role = models.TextField(null=True, blank=True, verbose_name='Роль')

    def __str__(self):
        return self.name


class Story(models.Model):
    comic = models.ForeignKey(Comic, related_name='stories')
    name = models.TextField(verbose_name='Наименование')
    type = models.TextField(null=True, blank=True, verbose_name='Тип')

    def __str__(self):
        return self.name


class Order(models.Model):
    comic = models.ForeignKey(Comic)
    email = models.TextField(verbose_name='Email')

    def __str__(self):
        return self.email
